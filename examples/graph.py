"""
Generate a GraphViz (dot) graph of follower relationships.

Original script by @cel:

#!/bin/sh
# This software is dedicated to the public domain.

read_contacts() {
	# Prefer sbotc because faster
	if command -v jq >/dev/null 2>&1; then
		sbotc messagesByType '{"reverse":true,"type":"contact"}'
	else
		sbot log --reverse --type contact
	fi
}

# Read the DB's contact messages in reverse (newest first).
# Uniquify by (author, target) since the latest is what counts.
# Omit unfollowed/unblocked state
read_contacts | jq -r ' .value|select(.content.contact|startswith("@")?)|[
	.author,
	.content.contact,
	(.content|(
		if .blocking or .flagged then -1
		elif .following == true then 1
		else 0 end
	))
]|@tsv' | awk '!seen[$1 $2]++ && $3 != 0'
"""

import logging
import struct
import time
from asyncio import get_event_loop, gather, ensure_future

from colorlog import ColoredFormatter

from secret_handshake.network import SHSClient
from ssb_client.muxrpc import MuxRPCAPI, MuxRPCAPIException
from ssb_client.packet_stream import PacketStream, PSMessageType
from ssb_client.util import load_ssb_secret

import hashlib
import base64


api = MuxRPCAPI()


async def test_client():

    response = await api.call("whoami", [], "sync")
    my_id = response.body["id"]
    print(my_id)

    import sqlite3

    conn = sqlite3.connect("graph.db")
    c = conn.cursor()
    # Create table
    c.execute("""CREATE TABLE IF NOT EXISTS connections (c_from text, c_to text)""")
    c.execute(
        "CREATE UNIQUE INDEX IF NOT EXISTS idx_connections_unique ON connections (c_from, c_to)"
    )

    seen = set()

    print("digraph {")
    async for msg in api.call(
        "messagesByType", [{"type": "contact", "reverse": True}], "source"
    ):
        try:
            author = msg.body["value"]["author"]
            content = msg.body["value"]["content"]
            contact = content["contact"]
        except:
            # ignore malformed messages
            continue
        # ignore invalid contacts
        if isinstance(contact, str) and (
            contact.endswith(".ed25519") or contact.endswith(".ggfeed-v1")
        ):
            relation = (author, contact)

            following = False
            if content.get("blocking") or content.get("flagged"):
                # print(content)
                pass
            elif content.get("following"):
                following = True
            if following and relation not in seen:
                print(f'"{author}" -> "{content["contact"]}";')
                c.execute("INSERT OR IGNORE INTO connections VALUES (?, ?)", relation)
                if len(seen) % 100 == 0:
                    conn.commit()
                seen.add(relation)

    conn.commit()

    print("}")


async def main():
    client = SHSClient("127.0.0.1", 8008, keypair, bytes(keypair.verify_key))
    packet_stream = PacketStream(client)
    await client.open()
    api.add_connection(packet_stream)
    await gather(ensure_future(api), test_client())


if __name__ == "__main__":
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    # create formatter
    formatter = ColoredFormatter(
        "%(log_color)s%(levelname)s%(reset)s:%(bold_white)s%(name)s%(reset)s - "
        "%(cyan)s%(message)s%(reset)s"
    )

    # add formatter to ch
    ch.setFormatter(formatter)

    keypair = load_ssb_secret()["keypair"]

    loop = get_event_loop()
    loop.run_until_complete(main())
    loop.close()
